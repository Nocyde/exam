## Quick start

Créer l'environnement virtuel :
```
cd server
python3 -m venv venv
```

Lancer l'environnement virtuel :
```
.\venv\scripts\activate
```

Installer les dépendances :
```
pip install -r requirements.txt
```

Lancement du serveur (rajouter --port:XXXX pour changer de port si vous le souhaitez mais veillez à changer les variables locales dans la partie client en conséquence)
```
uvicorn main:app --reload
```
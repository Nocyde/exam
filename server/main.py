from typing import Optional
from fastapi import FastAPI, Request

import requests

app = FastAPI()

def creer_deck():
    r = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle")
    rjson = r.json()
    return rjson["deck_id"]

def tirer_cartes(deck_id, nombre_cartes):
    r = requests.get("https://deckofcardsapi.com/api/deck/" + deck_id + "/draw/?count=" + str(nombre_cartes))
    rjson = r.json()
    #Gestion des erreurs (notamment quand il n'y a pas assez de cartes dans le deck pour en tirer)
    if rjson['success']== True :
        return rjson['cards']
    else:
        return rjson['error']

def melanger_deck(deck_id):
    r = requests.get("https://deckofcardsapi.com/api/deck/" + deck_id + "/shuffle/")
    rjson = r.json()
    return rjson['success']

@app.get("/")
def read_root():
    return {"Message": "Bienvenue dans mon TP noté :)"}

@app.get("/creer-un-deck")
def creer_deck_api():
    return creer_deck()

@app.post("/cartes/{nombre_cartes}")
async def tirer_cartes_api(request:Request, nombre_cartes):
    try : 
        r = await request.json()
    except :
        #Dans le cas où il n'y a pas de deck dans le body on en utilise un nouveau
        r = {
            "deck_id" : "new"
        }
    return tirer_cartes(r["deck_id"], nombre_cartes)

@app.post("/melanger")
async def melanger_deck_api(request:Request):
    try : 
        r = await request.json()
    except :
        r = {
            "deck_id" : "new"
        }
    return melanger_deck(r["deck_id"])


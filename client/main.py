import requests
from PyInquirer import prompt, print_json
from config import get_config

api_url= get_config()['API_URL']

def compte_couleurs(cards):
    try:
        dict_couleur = {"H" : 0, "S" : 0, "D" : 0, "C" : 0}
        for card in cards:
            dict_couleur[card['code'][-1]] += 1
        return dict_couleur
    except:
        return cards

def creer_un_deck():
    r = requests.get(api_url +"/creer-un-deck")
    print("Deck initialisé.\nID : " + str(r.json()))
    return r.json()

def piocher(_deck_id, nombre_cartes):
    try:
        r = requests.post(api_url + "/cartes/" + str(nombre_cartes), json = {"deck_id" : _deck_id})
        rjson = r.json()
        cards = ""
        try:
            for card in rjson:
                cards += "[" + card['code'] + "] "
            print(cards)
            return rjson
        except:
            print(rjson)
            return [] 
    except:
        print("Une erreur est survenue. Vérifiez que vous avez bien créé un deck et que vous demandez de tirer un nombre entier de cartes.")
        return {}

deck_id = None
pioche = []

menu_principal = [
    {
        'type' : 'list',
        'name' : 'choix',
        'message' : 'Que souhaitez-vous faire ?',
        'choices' : [
            'Créer un deck',
            'Piocher des cartes',
            'Compter les couleurs des dernières cartes piochées',
            'Mélanger le deck',
            'Executer le scénario',
            'Quitter'
        ]
    }
]

menu_piocher = [
    {
        'type' : 'input',
        'name' : 'nombre_cartes',
        'message' : "Combien de cartes souhaitez-vous piocher ?"
    }
]


if __name__ == "__main__":
    
    while(True):
    
        choix = prompt(menu_principal)

        if (choix['choix'] == 'Créer un deck'):
            deck_id = creer_un_deck()
        elif (choix['choix'] == 'Piocher des cartes'):
            choix_nombre = prompt(menu_piocher)
            pioche = piocher(deck_id, choix_nombre['nombre_cartes'])
        elif (choix['choix'] == 'Mélanger le deck'):
            try:
                r = requests.post(api_url + "/melanger", json = {"deck_id" : deck_id})
                rjson = r.json()
                if rjson == True:
                    print("Deck " + str(deck_id) +" bien mélangé")
                else :
                    print("Une erreur est survenue")
            except:
                print("Une erreur est survenue. Vérifier que vous avez déjà créé un deck")
        elif (choix['choix'] == 'Compter les couleurs des dernières cartes piochées'):
            print(compte_couleurs(pioche))

        elif (choix['choix'] == 'Executer le scénario'):
            exec (open('executable.py').read())

        elif (choix['choix'] == 'Quitter'):
            print("Au revoir")
            break
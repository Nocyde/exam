import pytest

from main import compte_couleurs

def test_compte_couleurs():
    data_test = [
        {'code': '9C', 'image': 'https://deckofcardsapi.com/static/img/9C.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/9C.svg', 'png': 'https://deckofcardsapi.com/static/img/9C.png'}, 'value': '9', 'suit': 'CLUBS'},
        {'code': '4C', 'image': 'https://deckofcardsapi.com/static/img/4C.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/4C.svg', 'png': 'https://deckofcardsapi.com/static/img/4C.png'}, 'value': '4', 'suit': 'CLUBS'}, 
        {'code': '3C', 'image': 'https://deckofcardsapi.com/static/img/3C.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/3C.svg', 'png': 'https://deckofcardsapi.com/static/img/3C.png'}, 'value': '3', 'suit': 'CLUBS'}, 
        {'code': 'JC', 'image': 'https://deckofcardsapi.com/static/img/JC.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/JC.svg', 'png': 'https://deckofcardsapi.com/static/img/JC.png'}, 'value': 'JACK', 'suit': 'CLUBS'}, 
        {'code': '2D', 'image': 'https://deckofcardsapi.com/static/img/2D.png', 'images': {'svg': 'https://deckofcardsapi.com/static/img/2D.svg', 'png': 'https://deckofcardsapi.com/static/img/2D.png'}, 'value': '2', 'suit': 'DIAMONDS'}, 
    ]
    
    resultat_attendu = {'H': 0, 'S': 0, 'D': 1, 'C': 4}

    assert compte_couleurs(data_test) == resultat_attendu


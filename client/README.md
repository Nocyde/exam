## Quick start

Création d'un environnement virtuel :
```
cd client
python3 -m venv venv
```

Lancer l'environnement virtuel :
```
.\venv\scripts\activate
```

Installer les dépendances :
```
pip install -r requirements.txt
```

Copier les valeurs de .env.local dans .env pour travailler localement

Lancement de l'application, dans un nouveau terminal dans exam :
```
python main.py
```

Lancer les tests unitaires (facultatif) :
```
pytest
```

Pour tester l'executable sans passer par l'application :

```
python executable.py
```
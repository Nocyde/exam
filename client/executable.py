from main import *

print("INITIALISATION D'UN DECK")
deck_id = creer_un_deck()

print("\nPIOCHE DE 10 CARTES")
pioche = piocher(deck_id, 10)

print("\nCOMPTAGE DES COULEURS")
print(compte_couleurs(pioche))
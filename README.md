# TP noté conception logicielle

## Objectif

Utilisation de l'API https://deckofcardsapi.com/

Le code est séparé en deux modules :

* Un webservice qui peut récupérer un deck, en tirer des cartes ou bien le mélanger.

* Une partie client qui permet à l'utilisateur de faire appel au webservice et d'initialiser un deck, de tirer des cartes, le mélanger et compter le nombre de couleurs des cartes piochées.


## Installation

Cloner le git et se placer dedans : 
```
git clone https://gitlab.com/Nocyde/exam
cd exam
```

## Partie serveur

Créer l'environnement virtuel :
```
cd server
python3 -m venv venv
```

Lancer l'environnement virtuel :
```
.\venv\scripts\activate
```

Installer les dépendances :
```
pip install -r requirements.txt
```

Lancement du serveur (rajouter --port:XXXX pour changer de port si vous le souhaitez mais veillez à changer les variables locales dans la partie client en conséquence)
```
uvicorn main:app --reload 
```

## Partie application

Création d'un environnement virtuel :
```
cd client
python3 -m venv venv
```

Lancer l'environnement virtuel :
```
.\venv\scripts\activate
```

Installer les dépendances :
```
pip install -r requirements.txt
```

Copier les valeurs de .env.local dans .env pour travailler localement

Lancement de l'application, dans un nouveau terminal dans exam :
```
python main.py
```

Lancer les tests unitaires (facultatif) :
```
pytest
```

Pour tester l'executable sans passer par l'application :

```
python executable.py
```